
import java.io.*;

/**
 * Created by anton on 22.04.17.
 */
public class MainClass {

    private final static String INPUT_FILE_NAME = "input.txt";
    private final static String OUTPUT_FILE_NAME = "output.txt";
    private static String[][] mArray = new String[0][0];
    private static int mArraySideSize = 0;
    private enum Direction {LEFT, UP, RIGHT, DOWN}

    public static void main(String[] args) {

        readArray();
        writeResult(transformArray());
    }




    private static void readArray() {

        File mInputFile = new File(INPUT_FILE_NAME);

        try (BufferedReader mBufferReader = new BufferedReader(new FileReader(mInputFile))) {
            String mLine;
            mLine = mBufferReader.readLine();

            if (mLine != null && !mLine.isEmpty()) {

                for (String ignored : mLine.split(" ")) {
                    mArraySideSize++;
                }

                mArray = new String[mArraySideSize][mArraySideSize];

                int i = 0;
                for (String mElemOfLine : mLine.split(" ")) {
                    mArray[0][i] = mElemOfLine;
                    i++;
                }

                int k = 1;
                while ((mLine = mBufferReader.readLine()) != null) {
                    i = 0;
                    for (String mElemOfLine : mLine.split(" ")) {
                        mArray[k][i] = mElemOfLine;
                        i++;
                    }
                    k++;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String transformArray() {
        String mTempResult = "";
        if (mArray.length!=0) {

            int i = mArraySideSize / 2;
            int j = i;

            int min_i = i;
            int max_i = i;
            int min_j = j;
            int max_j = j;

            Direction mCurrentDirection = Direction.LEFT;

            for (int a = 0; a < mArraySideSize*mArraySideSize; a++) {
                mTempResult = mTempResult.concat(mArray[j][i]);
                mTempResult = mTempResult.concat(" ");

                //System.out.print(mArray[j][i]+ " ");

                switch (mCurrentDirection) {
                    case LEFT:
                        i -= 1;
                        if (i < min_i) {
                            mCurrentDirection = Direction.DOWN;
                            min_i = i;
                        }
                        break;
                    case UP:
                        j -= 1;
                        if (j < min_j) {
                            mCurrentDirection = Direction.LEFT;
                            min_j = j;
                        }

                        break;
                    case RIGHT:
                        i += 1;
                        if (i > max_i) {
                            mCurrentDirection = Direction.UP;
                            max_i = i;
                        }
                        break;
                    case DOWN:
                        j += 1;
                        if (j > max_j) {
                            mCurrentDirection = Direction.RIGHT;
                            max_j = j;
                        }
                        break;

                }
            }
        }

        return mTempResult;
    }


    private static void writeResult(String result) {

        File output = new File(OUTPUT_FILE_NAME);

        try (FileOutputStream os = new FileOutputStream(output)) {
            os.write(result.getBytes());
            os.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
